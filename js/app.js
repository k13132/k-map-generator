"use strict";

function sortNumber(a,b) {
    return a - b;
}

function generate() {
    $("#svg").empty();
    $(".output-container").css('display', 'none');
    $("#ascii-container").css('display', 'none');
    $("#ascii").height(10);
    
    let func = $("#funcIN").val();
    
    if(func.length == 0) {
        $('#alert').css('display', '');
        $('#alert-text').text('Nebyla zadána žádná funkce');
        return;
    }
    
    func = func.replace(/\./g, ',');
    func = func.replace(/\s/g, '');
    //console.log(func);

    //console.log(func.match(/^(?:\d+,)*\d$/));
    
    if(func.match(/^(?:\d+,)*\d+$/) == null) {
        $('#alert').css('display', '');
        $('#alert-text').text('Funkce nemá vhodný formát');
        return;
    }
    
    //input function OK
    $('#alert').css('display', 'none');
    
    let funcA = func.split(",").sort(sortNumber);
    //console.log(funcA);
    
    let options = {
        varCount : 0,
    };
    
    let minimal;
    
    //console.log(funcA[funcA.length - 1]);
        
    if(funcA[funcA.length - 1] > 31) { // exceeds maximum
        $('#alert').css('display', '');
        $('#alert-text').text('Funkce obsahuje index, který je mimo povolený rozsah (0-31)');
        $('#funcR').text('f = ' + func);        
        $("#funcR-container").css('display', '');
        return;
    } else if(funcA[funcA.length - 1] > 15) { // 5 var
        options.varCount = 5;
    } else if(funcA[funcA.length - 1] > 7) { // 4 var
        options.varCount = 4;
    } else { //3 var
        options.varCount = 3;
    }
    
    //show output containers
    $(".output-container").css('display', '');    
    if($("#chb-ascii").prop("checked")) {
        $("#ascii-container").css('display', '');
    }    
    
    
    let map = makeMap(funcA, options);
    minimal = minimize(funcA, options);
    
    //console.log(minimal);
    
    $('#result').html(minimal);
    
    switch(options.varCount) {
        case 5 : {
            drawASCII5(map, funcA);
            drawSVG5(map, options);
            break;
        }
            
        case 4 : {
            drawASCII4(map, funcA);
            drawSVG4(map, options);
            break;
        }
            
        case 3 : {
            drawASCII3(map, funcA);
            drawSVG3(map, options);
            break;
        }
    }
    
    $("#ascii").height($("#ascii")[0].scrollHeight);    
}

let indexMap = 
        [
            [
                [0,1,3,2],
                [4,5,7,6]
            ],
            [
                [0,1,3,2],
                [4,5,7,6],
                [12,13,15,14],
                [8,9,11,10]
            ],
            [
                [0,1,3,2,6,7,5,4],
                [8,9,11,10,14,15,13,12],
                [24,25,27,26,30,31,29,28],
                [16,17,19,18,22,23,21,20]
            ]
        ];

function makeMap(funcA, options) {
    
    let varIndex = options.varCount - 3;    
    
    let dims = 
        [
            {y: 2, x: 4},
            {y: 4, x: 4},
            {y: 4, x: 8}
        ]
    
    let map = {
        map : [],
        funcR : [],
        funcRS : ""
    };
    //generate map
    
    let l = 0;
    //let funcR = "f = ";    
    
    for(let i = 0; i < dims[varIndex].y; i++) {
        map.map[i] = [];
        for(let j = 0; j < dims[varIndex].x; j++) {
            map.map[i][j] = 0;
            
            for(let k = 0; k < funcA.length; k++) {
                if(indexMap[varIndex][i][j] == funcA[k]) {
                    map.funcR[l++] = funcA[k];// += funcA[k] + ', ';
                    map.map[i][j] = 1;
                    break;
                }
            }
            
            //console.log(i + '; ' + j);
        }
    }
    
    //create real input function
    map.funcR = map.funcR.sort(sortNumber);
    map.funcRS = map.funcR.join(', ');
    
    $('#funcR').text('f = ' + map.funcRS);
    
    //console.log(map);
    
    return map;
}

//function generate5(funcA, options) {
function drawASCII5(map, funcA) {    
    let startMap = [
        ["   ", "   "],
        [" | ", " | "],
        ["|d ", "|| "],
        ["e| ", "|  "]
    ];
    
    let output = "f=" + map.funcRS + "\n\n";
    output += "                   ---------c-------\n           ----------b------        \n       ----a----       ----a----    \n";
    
    for(let i = 0, j = 0; i < 4; i++) {
        output += startMap[i][0] + "+---+---+---+---+---+---+---+---+\n" + startMap[i][1] + "|";
        for(j = 0; j < 8; j++) {
            if((map.map[i][j] == 1)) {
                output += " 1 |"
            } else {
                output += "   |"
            }
        }
        output += "\n";
    }
    
    output = output + "|  +---+---+---+---+---+---+---+---+";
    
    //console.log(output);
    $("#ascii").text(output);
}

function drawSVG5(map, options) {    
    let varIndex = options.varCount - 3;
    
    //draw SVG
    let from_left = 35, from_top = 70;
    
    let draw = SVG('svg').size(from_left+50*8+2, from_top+50*4+2+15);
    
    let policko, rect, text, variable;   
    
    for(let i = 0, j; i < 4; i++) {
        for(j = 0; j < map.map[0].length; j++) {
            policko = draw.group();

            rect = draw.rect(50,50).fill('none').stroke({ width: 1, color: 'black' });
            policko.add(rect);

            text = draw.text(map.map[i][j].toString());
            text.font({size: 30, family: 'Helvetica'}).center(25,25);
            policko.add(text);

            text = draw.text(indexMap[varIndex][i][j].toString());
            text.font({size: 13, family: 'Helvetica'}).center(41,40);    
            policko.add(text);

            policko.move((j*50)+from_left,i*50+from_top);
        }    
    }
        
    //a-1
    draw.line(from_left+50*1, from_top-5, from_left+50*3, from_top-5).stroke({ width: 2 });
    variable = draw.text('a').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*3-4-variable.length(),from_top-5-15-4);
    
    //a-2
    draw.line(from_left+50*5, from_top-5, from_left+50*7, from_top-5).stroke({ width: 2 });
    variable = draw.text('a').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*7-4-variable.length(),from_top-5-15-4);
    
    //b
    draw.line(from_left+50*2, from_top-25, from_left+50*6, from_top-25).stroke({ width: 2 });
    variable = draw.text('b').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*6-4-variable.length(),from_top-25-15-4);
    
    //c
    draw.line(from_left+50*4, from_top-50, from_left+50*8, from_top-50).stroke({ width: 2 });
    variable = draw.text('c').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*8-4-variable.length(),from_top-50-15-4);
    
    //d
    draw.line(from_left-5, from_top+50*1, from_left-5, from_top+50*3).stroke({ width: 2 });
    variable = draw.text('d').font({size: 15, family: 'Helvetica'});
    variable.move(from_left-5-4-variable.length(),from_top+50*3-15);
    
    //e
    draw.line(from_left-5-15, from_top+50*2, from_left-5-15, from_top+50*4).stroke({ width: 2 });
    variable = draw.text('e').font({size: 15, family: 'Helvetica'});
    variable.move(from_left-5-15-4-variable.length(),from_top+50*4-15);
    
    //delici cara
    for(let i = 0; i < 12; i += 2) {
        draw.line(from_left+50*4, from_top-50+(5*(i)), from_left+50*4, from_top-50+(5*(i+1))).stroke({ width: 1,  });
    }    
    for(let i = 0; i < 4; i += 2) {
        draw.line(from_left+50*4, from_top+50*4+(5*(i)), from_left+50*4, from_top+50*4+(5*(i+1))).stroke({ width: 1,  });
    }
}

function drawASCII4(map, funcA) {
    
    let startMap = [
        ["   ", "   "],
        [" | ", " | "],
        ["|c ", "|| "],
        ["d| ", "|  "]
    ];
            
    let output = "f=" + map.funcRS + "\n\n";
    output += "           ----b----\n       ----a----    \n";
    
    for(let i = 0, j = 0; i < 4; i++) {
        output += startMap[i][0] + "+---+---+---+---+\n" + startMap[i][1] + "|";
        for(j = 0; j < 4; j++) {
            if((map.map[i][j] == 1)) {
                output += " 1 |"
            } else {
                output += "   |"
            }
        }
        output += "\n";
    }
    
    output = output + "|  +---+---+---+---+";
    
    //console.log(output);
    $("#ascii").text(output);
}

function drawSVG4(map, options) {
    let varIndex = options.varCount - 3;
    
    //draw SVG
    let from_left = 35, from_top = 30;
    
    let draw = SVG('svg').size(from_left+50*4+2, from_top+50*4+2);
    
    let policko, rect, text, variable;   
    
    for(let i = 0, j; i < 4; i++) {
        for(j = 0; j < map.map[0].length; j++) {
            policko = draw.group();

            rect = draw.rect(50,50).fill('none').stroke({ width: 1, color: 'black' });
            policko.add(rect);

            text = draw.text(map.map[i][j].toString());
            text.font({size: 30, family: 'Helvetica'}).center(25,25);
            policko.add(text);

            text = draw.text(indexMap[varIndex][i][j].toString());
            text.font({size: 13, family: 'Helvetica'}).center(41,40);    
            policko.add(text);

            policko.move((j*50)+from_left,i*50+from_top);
        }    
    }
    
    //a
    draw.line(from_left+50, from_top-15, from_left+50*3, from_top-15).stroke({ width: 2 });
    variable = draw.text('a').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*3-4-variable.length(),from_top-15-15-4);
    
    //b
    draw.line(from_left+50*2, from_top-5, from_left+50*4, from_top-5).stroke({ width: 2 });
    variable = draw.text('b').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*4-4-variable.length(),from_top-5-15-4);
    
    //c
    draw.line(from_left-5, from_top+50*1, from_left-5, from_top+50*3).stroke({ width: 2 });
    variable = draw.text('c').font({size: 15, family: 'Helvetica'});
    variable.move(from_left-5-4-variable.length(),from_top+50*3-15);
    
    //d
    draw.line(from_left-5-15, from_top+50*2, from_left-5-15, from_top+50*4).stroke({ width: 2 });
    variable = draw.text('d').font({size: 15, family: 'Helvetica'});
    variable.move(from_left-5-15-4-variable.length(),from_top+50*4-15);
}

function drawASCII3(map, funcA) {
    let startMap = [
        ["  ", "  "],
        ["| ", "c "]
    ];        
    let row = 0;
            
    let output = "f=" + map.funcRS + "\n\n";
    output += "          ----b----\n      ----a----    \n";
    
    for(let i = 0, j = 0; i < 2; i++) {
        output += startMap[i][0] + "+---+---+---+---+\n" + startMap[i][1] + "|";
        for(j = 0; j < 4; j++) {
            if((map.map[i][j] == 1)) {
                output += " 1 |"
            } else {
                output += "   |"
            }
        }
        output += "\n";
    }
    
    output = output + "| +---+---+---+---+";
    
    //console.log(output);
    $("#ascii").text(output);
}

function drawSVG3(map, options) {
    let varIndex = options.varCount - 3;
    
    //draw SVG
    let from_left = 20, from_top = 30;
    
    let draw = SVG('svg').size(from_left+50*4+2, from_top+50*2+2);
    
    let policko, rect, text, variable;   
    
    for(let i = 0, j; i < 2; i++) {
        for(j = 0; j < map.map[0].length; j++) {
            policko = draw.group();

            rect = draw.rect(50,50).fill('none').stroke({ width: 1, color: 'black' });
            policko.add(rect);

            text = draw.text(map.map[i][j].toString());
            text.font({size: 30, family: 'Helvetica'}).center(25,25);
            policko.add(text);

            text = draw.text(indexMap[varIndex][i][j].toString());
            text.font({size: 13, family: 'Helvetica'}).center(41,40);    
            policko.add(text);

            policko.move((j*50)+from_left,i*50+from_top);
        }    
    }
    
    //a
    draw.line(from_left+50, from_top-15, from_left+50*3, from_top-15).stroke({ width: 2 });
    variable = draw.text('a').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*3-4-variable.length(),from_top-15-15-4);
    
    //b
    draw.line(from_left+50*2, from_top-5, from_left+50*4, from_top-5).stroke({ width: 2 });
    variable = draw.text('b').font({size: 15, family: 'Helvetica'});
    variable.move(from_left+50*4-4-variable.length(),from_top-5-15-4);
    
    //c
    draw.line(from_left-5, from_top+50*1, from_left-5, from_top+50*2).stroke({ width: 2 });
    variable = draw.text('c').font({size: 15, family: 'Helvetica'});
    variable.move(from_left-5-4-variable.length(),from_top+50*2-15);
}

$(document).ready(function() {
    $("#gen").click(generate);
    
    $("#btn-save-as-svg").click( function() {
        let text = $("#svg").html();
        let filename = "K-Map-" + new Date().getTime();
        let blob = new Blob([text], {type: "image/svg+xml;charset=utf-8"});
        saveAs(blob, filename+".svg");
    });
});






























