"use strict";

// from http://stackoverflow.com/a/11454049/309483

function combine(m, n) {
    let a = m.length, c = '', count = 0, i;
    for (let i = 0; i < a; i++) {
        if (m[i] === n[i]) {
            c += m[i];
        } else if (m[i] !== n[i]) {
            c += '-';
            count += 1;
        }
    }

    if (count > 1) {
        return "";
    }

    return c;
}

function repeatelem(elem, count) {
    let accu = [],
        addOneAndRecurse = function(remaining) { accu.push(elem); if (remaining > 1) { addOneAndRecurse(remaining - 1); } };
    addOneAndRecurse(count);
    return accu;
}

function find_prime_implicants(data) {
    let newList = [].concat(data),
        size = newList.length,
        IM = [],
        im = [],
        im2 = [],
        mark = repeatelem(0, size),
        mark2,
        m = 0,
        i,
        j,
        c,
        p,
        n,
        r,
        q;
    for (let i = 0; i < size; i++) {
        for (j = i + 1; j < size; j++) {
            c = combine(newList[i], newList[j]);
            if (c !== "") {
                im.push(c);
                mark[i] = 1;
                mark[j] = 1;
            }
        }
    }

    mark2 = repeatelem(0, im.length);
    for (let p = 0; p < im.length; p++) {
        for (let n = p + 1; n < im.length; n++) {
            if (p !== n && mark2[n] === 0 && im[p] === im[n]) {
                mark2[n] = 1;
            }
        }
    }

    for (let r = 0; r < im.length; r++) {
        if (mark2[r] === 0) {
            im2.push(im[r]);
        }
    }

    for (let q = 0; q < size; q++) {
        if (mark[q] === 0) {
            IM.push(newList[q]);
            m = m + 1;
        }
    }

    if (m !== size && size !== 1) {
        IM = IM.concat(find_prime_implicants(im2));
    }

    IM.sort();
    return IM;
}

//['1101', '1100', '1110', '1111', '1010', '0011', '0111', '0110']
function minimize(funcA, options) {
    let toBin =  [
                    [
                        '000','001','010','011','100','101','110','111'
                    ],
                    [
                        '0000','0001','0010','0011','0100','0101','0110','0111',
                        '1000','1001','1010','1011','1100','1101','1110','1111'
                    ],
                    [
                        '00000','00001','00010','00011','00100','00101','00110','00111',
                        '01000','01001','01010','01011','01100','01101','01110','01111',
                        '10000','10001','10010','10011','10100','10101','10110','10111',
                        '11000','11001','11010','11011','11100','11101','11110','11111'
                    ]
                 ];
    
    let names = [
        ['c', 'b', 'a'],
        ['d', 'c', 'b', 'a'],
        ['e', 'd', 'c', 'b', 'a']
    ]
    
    let mintermsIn = [];
    
    for(let i = 0; i < funcA.length; i++) {
        mintermsIn[i] = toBin[options.varCount - 3][funcA[i]];
    }
    
    let minimal = find_prime_implicants(mintermsIn);
    
    //console.log(minimal);
    
    //<span class='overline'>text</span>
    
    let output = "";
    
    for(let i = 0; i < minimal.length; i++) {
        for(let j = options.varCount - 1; j >= 0; j--) {
            switch(minimal[i].charAt(j)) {
                case '0' : {
                    output += "<span class='overline'>" + names[options.varCount - 3][j] + "</span>";
                    break;
                }
            
                case '1' : {
                    output += names[options.varCount - 3][j];
                    break;
                }
            }
        }
        
        if(i < (minimal.length - 1)) {
            output += " + ";
        }
    }

    return output;
}

/*
var minterms = ['1101', '1100', '1110', '1111', '1010', '0011', '0111', '0110'];
var minterms2 = ['0000', '0100', '1000', '0101', '1100', '0111', '1011', '1111'];
var minterms3 = ['0001', '0011', '0100', '0110', '1011', '0000', '1000', '1010', '1100', '1101'];
console.log( 'PI(s):', JSON.stringify(find_prime_implicants(minterms)));
console.log( 'PI2(s):', JSON.stringify(find_prime_implicants(minterms2)));
console.log( 'PI3(s):', JSON.stringify(find_prime_implicants(minterms3)));
*/